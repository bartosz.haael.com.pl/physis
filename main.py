#!/usr/bin/python3
#-*- coding:utf-8 -*-


class Meta(type):
	def mro(cls):
		mro = super(Meta, cls).mro()
		try:
			mro.remove(A)
			mro.insert(0, A)
		except NameError:
			pass
		return mro


class A(metaclass=Meta):
	field = 'A'
	
	def method(self):
		return 'A'

class B(A):
	#field = 'B'
	subfield = 'B'
	
	def method(self):
		return 'B'
	
	def __init__(self):
		self.field = 'B'
		self.member = 'B'


class C(A):
	#field = 'C'
	subfield = 'C'
	
	def method(self):
		return 'C'
	
	def __init__(self):
		self.field = 'C'
		self.member = 'C'


class D(A, B):
	#field = 'D'
	subfield = 'D'
	
	def method(self):
		return 'D'
	
	def __init__(self):
		self.field = 'D'
		self.member = 'D'


#class E(B, A):
#	pass

class F(B, C):
	#field = 'F'
	subfield = 'F'
	
	def method(self):
		return 'F'
	
	def __init__(self):
		self.field = 'F'
		self.member = 'F'


class G(C, B):
	#field = 'G'
	subfield = 'G'
	
	def method(self):
		return 'G'
	
	def __init__(self):
		self.field = 'G'
		self.member = 'G'


print("class field:", A.field, B.field, C.field, D.field, F.field, G.field)
print("class subfield:", B.subfield, C.subfield, D.subfield, F.subfield, G.subfield)
print("instance field:", A().field, B().field, C().field, D().field, F().field, G().field)
print("instance member:", B().member, C().member, D().member, F().member, G().member)
print("instance class:", A().__class__, B().__class__, C().__class__, D().__class__, F().__class__, G().__class__)





quit()


from types import FunctionType, MethodType


class Metaclass(type):
	def __new__(cls, name, bases, attrs):
		print("metaclass", name, bases, attrs)

		for symbol in ['fn']:
			originals = []
			try:
				originals.append(attrs[symbol])
			except KeyError:
				pass
			
			for base in bases:
				try:
					originals.append(base.fn)
				except AttributeError:
					pass
			
			def proceed(): # make closure
				function = cls.Sequential(originals)
				attrs[symbol] = lambda *args, **kwargs: function(*args, **kwargs)
			proceed()
		
		return type.__new__(cls, name, bases, attrs)
	
	class Sequential:
		def __init__(self, originals):
			self.__originals = originals
		
		def __call__(self, *args, **kwargs):
			#print("sequential", self.__originals, args, kwargs)
			for original in self.__originals:
				original(*args, **kwargs)


def important(method):
	method.important = True
	return method


#class I(metaclass=Metaclass):
class I:
	@important
	def fn(self, *args, **kwargs):
		print("self:", self)
		print(set(Class.fn for Class in self.__class__.__mro__ if not issubclass(I, Class)))
	
	def __getattribute__(self, attr):
		if attr[0] == '_':
			return object.__getattribute__(self, attr)
		elif attr in self.__important:
			field = getattr(I, attr)
			print("getattr", attr, field)
			if isinstance(field, FunctionType):
				print(dir(field))
				return MethodType(field, self)
			else:
				return field
		else:
			return object.__getattribute__(self, attr)

I._I__important = []
for name, value in I.__dict__.items():
	if hasattr(value, 'important'):
		I._I__important.append(name)


class A(I):
	def fn(self):
		print("A.fn")

class B(I):
	def fn(self):
		print("B.fn")

class Z(A, B):
	def fn(self):
		print("Z.fn")


z = Z()
z.fn()

class X:
	def __init__(self):
		self.z = 1
	
	def x(self):
		pass
	
	y = 0

x = X()

print(X.x, x.x)
print(X.y, x.y)
print(x.z)

quit()



#from __future__ import unicode_literals



from cmath import sqrt, exp, pi
from physis.algebra import *
from physis.physics import *
from physis.interactions import *


class Field:
	@classmethod
	def EXP(Class, origin, momentum):
		" exp(1j * momentum * (position - origin)) "
		pass
	
	@classmethod
	def COS(Class, origin, frequency):
		" cos(frequency * abs(position - origin)) "
		pass
	
	@classmethod
	def POINT(Class, origin):
		pass
	
	@classmethod
	def GAUSS(Class, origin, variance):
		pass
	
	@classmethod
	def BALL(Class, origin, radius):
		pass
	
	def __init__(self, content):
		self.__content = content
	
	def __getitem__(self, selector):
		"""
		d[c] - value in point c (equality comparison)
		d[c::dx] - value around point c (proximity comparison)
		d[c:r:dx] - integral over a sphere of radius r
		d[::dx] - integral over the whole space
		"""
		if isinstance(selector, slice):
			start = selector.start
			stop = selector.stop
			step = selector.step
		else:
			result = 0
			for (origin, radius, value) in self.__content:
				if origin == selector:
					result += value
			return result
	
	def __add__(self, other):
		pass
	
	def __sub__(self, other):
		pass
	
	def __neg__(self, other):
		pass
	
	def __mul__(self, other):
		pass
	
	def __rmul__(self, other):
		pass
	
	def __inv__(self):
		precision = 0.0001
		return self.fourier(Vector.ZERO, precision)
	
	def fourier(self, origin, dx):
		pass



'''
def ft3d(density, rang, ds):
	transform = ...
	x = -rang
	while x <= rang:
		y = -rang
		while y <= rang:
			z = -rang
			while z <= rang:
				transform[x, y, z] = density[x, y, z]
				z += ds
			y += ds
		x += ds
'''



precision = 1.e-16

def round0(c):
	if abs(c) < precision:
		return 0
	elif abs(c.real) < precision:
		return 1j * c.imag
	elif abs(c.imag) < precision:
		return c.real
	else:
		return c


def xfft1(data, c, dx, n):
	if n <= 0:
		return [data[c]]
	even = xfft1(data, c - dx/2, 2 * dx, n - 1)
	odd  = xfft1(data, c + dx/2, 2 * dx, n - 1)
	m = 2 ** n
	return [even[k] + exp(-2j * pi * k / m) * odd[k] for k in range(m // 2)] + \
	        [even[k] - exp(-2j * pi * k / m) * odd[k] for k in range(m // 2)]


def fft(data, c, dx, n):
	f = xfft1(data, c, dx, n)
	return [round0(_x / sqrt(len(f))) for _x in f]


'''
def xfft(x, s):
	n = len(x)
	if n <= 1: return x
	even = xfft(x[0::2], s)
	odd =  xfft(x[1::2], s)
	return [even[k] + exp(s * 2j * pi * k / n) * odd[k] for k in range(n // 2)] + \
	        [even[k] - exp(s * 2j * pi * k / n) * odd[k] for k in range(n // 2)]


def fft(x):
	return [round0(_c) for _c in [_x / sqrt(len(x)) for _x in xfft(x, -1)]]

def ifft(x):
	return [round0(_c) for _c in [_x / sqrt(len(x)) for _x in xfft(x, 1)]]

'''

class Interpolate:
	def __init__(self, lo, hi, data):
		self.__data = data
		self.__lo = lo
		self.__hi = hi
	
	def __getitem__(self, x):
		p = len(self.__data) * (x - self.__lo) / (self.__hi - self.__lo)
		q = int(p)
		if q < 0:
			return self.__data[0]
		elif q > len(self.__data) - 2:
			return self.__data[len(self.__data) - 1]
		else:
			c = p - q
			return self.__data[q] * (1 - c) + self.__data[q + 1] * c

signal = Interpolate(-8, 8, [0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0])

print([signal[x] for x in range(-8, 8)])

trans = fft(signal, 0, 0.5, 4)
#revs = xfft1(signal)

print(trans)
#print([abs(_x) for _x in trans])
#print(revs)


quit()


def random_orientation():
	oa = Vector(random.uniform(-5, 5), random.uniform(-5, 5), random.uniform(-5, 5)).vertor()
	ob = Vector(random.uniform(-5, 5), random.uniform(-5, 5), random.uniform(-5, 5)).vertor()
	oc = oa % ob
	ob = (oa + oa % (oc * random.uniform(0.5, 2)) + ob + ob % (oc * random.uniform(0.5, 2))).vertor()
	oa = ob % oc
	ok = Matrix(*([_x for _x in oa] + [_x for _x in ob] + [_x for _x in oc]))
	o1, _e, o2 = ok.singular_value_decomposition()
	o = o1 * o2
	return o


Universe = create_universe(Gravity, Electromagnetism)

universe = Universe(Gravity=(0.0,), Electromagnetism=(50.0, 50.0))

for nn in range(16):
	m = random.uniform(0.00001, 10)
	e = random.uniform(-5, 5)
	p = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
	v = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
	i = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
	o = random_orientation()
	r = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
	atom = universe.create(Atom(m, i, electric_charge=e), position=p, velocity=v, orientation=o, rotation=r)

for nn in range(1000):
	universe.tick(0.01)
	for n, item in enumerate(universe):
		print(n, item.position, item.velocity)
	print()



#!/usr/bin/python3
#-*- coding:utf-8 -*-


from __future__ import unicode_literals

import random
import math

from .helpers import *
from .algebra import *


def important(method):
	method.important = True
	return method

class Object:
	mass        = 0
	inertmoment = Vector.ZERO
	#deformation = Vector.ZERO
	
	position     = Vector.ZERO
	velocity     = Vector.ZERO
	acceleration = Vector.ZERO
	
	orientation = Matrix.UNIT
	rotation    = Vector.ZERO
	accelmoment = Vector.ZERO
	
	def __init__(self):
		raise NotImplementedError("This class can not be directly instantiated.")
	
	def __getattribute__(self, attr):
		if attr[0] == '_':
			return object.__getattribute__(self, attr)
		elif attr in self.__important
			return getattr(object.__class__, attr)
		else:
			return object.__getattribute__(self, attr)
	
	def __len__(self):
		try:
			return len(self.items)
		except AttributeError:
			return 0
	
	def __iter__(self):
		try:
			for item in self.items:
				yield item
		except AttributeError:
			pass
	
	'''
	def __getitem__(self, n):
		try:
			return self.item[n]
		except AttributeError:
			raise IndexError("This object cannot have sub-items.")
	
	def __setitem__(self, n, v):
		raise TypeError("Items can not be added to this object this way.")
	
	def __delitem__(self, n):
		raise TypeError("Items can not be deleted from this object this way.")
	'''
	
	def __contains__(self, item):
		try:
			return item in self.items
		except AttributeError:
			return False
	
	def container(self):
		return hasattr(self, 'items')
	
	def __submethod(self, symbol):
		functions = []
		for Class in self.__class__.__mro__:
			if issubclass(Object, Class): continue
			function = getattr(Class, symbol)
			if function not in functions:
				functions.append(function)
		return functions
	
	@important
	def equations(self, precision=1.e-12):
		correctness = True
		for submethod in self.__submethod('equations'):
			correctness = correctness and submethod(self, precision)
		return correctness
	
	@important
	def correction(self, precision=1.e-12):
		for submethod in self.__submethod('correction'):
			submethod(self, precision)
	
	@important
	def external_frame(self, srcitem, dstitem):
		"Change item parameters to the external frame. `srcitem` and `dstitem` may be the same object."
		
		ext_pos = self.orientation * srcitem.position
		
		position     = self.position + ext_pos
		velocity     = self.velocity + self.orientation * srcitem.velocity + self.rotation % ext_pos
		acceleration = self.acceleration + self.orientation * srcitem.acceleration + self.rotation % (self.rotation % (self.orientation * srcitem.position)) + 2 * self.rotation % (self.orientation * srcitem.velocity) + self.accelmoment % ext_pos
		orientation  = self.orientation * srcitem.orientation
		rotation     = self.rotation + srcitem.rotation.cross_product_rotation(self.orientation)
		angularaccel = self.angularaccel + srcitem.angularaccel.cross_product_rotation(self.orientation) + self.orientation.precession(self.rotation, srcitem.rotation)
		
		dstitem.position     = position
		dstitem.velocity     = velocity
		dstitem.acceleration = acceleration
		dstitem.orientation  = orientation
		dstitem.rotation     = rotation
		dstitem.angularaccel = angularaccel
	
	@important
	def internal_frame(self, srcitem, dstitem):
		"Change item parameters to the internal frame. `srcitem` and `dstitem` may be the same object."
		
		frame_orient_t = self.orientation.transpose()
		ext_pos = srcitem.position - self.position
		ext_vel = srcitem.velocity - self.rotation % ext_pos - self.velocity
		
		position     = frame_orient_t * ext_pos
		velocity     = frame_orient_t * ext_vel
		acceleration = frame_orient_t * (srcitem.acceleration - self.rotation % (self.rotation % ext_pos) - 2 * self.rotation % ext_vel - self.accelmoment % ext_pos - self.acceleration)
		orientation  = frame_orient_t * srcitem.orientation
		rotation     = (srcitem.rotation - self.rotation).cross_product_rotation(self.orientation.transpose())
		angularaccel = (srcitem.angularaccel - self.angularaccel - self.orientation.precession(self.rotation, rotation)).cross_product_rotation(self.orientation.transpose())
		
		dstitem.position     = position
		dstitem.velocity     = velocity
		dstitem.acceleration = acceleration
		dstitem.orientation  = orientation
		dstitem.rotation     = rotation
		dstitem.angularaccel = angularaccel
	
	@important
	def force(self, item, precision=1.e-12):
		force = Vector.ZERO
		for submethod in self.__submethod('force'):
			force += submethod(self, item, precision)
		return force
	
	@important
	def torque(self, item, precision=1.e-12):
		torque = Vector.ZERO
		for submethod in self.__submethod('torque'):
			torque += submethod(self, item, precision)
		return torque
	
	@important
	def dynamics(self, precision=1.e-12):
		for submethod in self.__submethod('dynamics'):
			submethod(self, item, precision)
	
	@important
	def kinematics(self, dt, precision=1.e-12):
		for submethod in self.__submethod('kinematics'):
			submethod(self, dt, precision)
	
	def tick(self, dt, precision=1.e-12):
		self.dynamics(precision=precision)
		self.kinematics(dt, precision=precision)
		if self.container():
			for item in self:
				item.tick(dt, precision=precision)
		if not self.equations():
			self.correction()

Object._Object__important = []
for name, value in Object.__dict__.items():
	if hasattr(value, 'important'):
		Object._Object__important.append(name)


class Shadow(Object):
	"""
	This class allows to create a `shadow` of an object. It can be used and modified as
	any other object while the original remains unaffected.
	"""
	
	def __init__(self, original):
		self.__original = original
		self.__deleted = set()
		self.__items = dict()
	
	def __eq__(self, item):
		orig1 = self.__original
		while True:
			try:
				orig1 = orig1.__original
			except AttributeError:
				break
		orig2 = item
		while True:
			try:
				orig2 = orig2.__original
			except AttributeError:
				break
		return orig1 is orig2
	
	def __getattr__(self, attr):
		if attr in self.__deleted:
			raise AttributeError("No such attribute.")
		return getattr(self.__original, attr)
	
	def __setattr__(self, attr, val):
		object.__setattr__(self, attr, val)
		try:
			self.__deleted.remove(attr)
		except KeyError:
			pass
	
	def __delattr__(self, attr):
		try:
			object.__delattr__(self, attr)
		except AttributeError:
			if attr in self.__deleted:
				raise AttributeError("No such attribute.")
		self.__deleted.add(attr)
	
	def __getitem__(self, key):
		try:
			return self.__items[key]
		except KeyError:
			item = self.__original[key]
			if isinstance(item, Object):
				shadow = Shadow(item)
				self.__items[key] = shadow
				return shadow
			else:
				return item
	
	#TODO: setitem, delitem


class Atom(Object):
	"Atom: an object that has no sub-items. Atoms are leaves of object tree."
	
	def __init__(self, mass, inertmoment, **kwargs):
		self.mass        = mass        # this body mass
		self.inertmoment = inertmoment # principal moments of inertia
		
		for k, v in kwargs.items():
			setattr(self, k, v)


class System(Object):
	"System: an object that can be a subitem and can have subitems. Systems are internal nodes of object tree."
	
	def __init__(self):
		self.items = []
		
		self.mass        = 0
		self.inertmoment = Vector.ZERO
		#self.deformation = Vector.ZERO
	
	def equations(self, precision=1.e-12):
		correctness = True
		correctness = correctness and sum((item.mass * item.position for item in self), Vector.ZERO) < precision
		correctness = correctness and sum((item.mass * item.velocity for item in self), Vector.ZERO) < precision
		correctness = correctness and sum((item.mass * item.acceleration for item in self), Vector.ZERO) < precision
		#TODO
		#sum((item.mass * item.position % item.velocity for item in self), Vector.ZERO) < precision
		#sum((item.mass * item.acceleration for item in self), Vector.ZERO) < precision
		return correctness
		"""
		∑ M_int[i] X_int[i] = 0
		∑ M_int[i] V_int[i] = 0
		∑ M_int[i] A_int[i] = 0
		∑ (M_int[i] X_int[i] × V_int[i] + I_int[i] R_int[i]) = 0
		∑ (M_int[i] X_int[i] × A_int[i] + I_int[i] T_int[i] + ∂I_int[i] R_int[i]) = 0
		"""
	
	def correction(self, precision=1.e-12): #TODO: optimize this method
		# calculate linear corrections
		mass     = 0
		center   = Vector.ZERO
		momentum = Vector.ZERO
		
		for item in self.items:
			mass     += item.mass
			center   += item.mass * item.position
			item.velocity += self.orientation.transpose() * (self.rotation % (self.orientation * item.position))
			momentum += item.mass * item.velocity
		
		position = center / mass
		velocity = momentum / mass
		
		# update item parameters
		for item in self.items:
			item.position -= position
			item.velocity -= velocity + self.orientation.transpose() * (self.rotation % (self.orientation * item.position))
		
		# update system parameters
		self.mass     = mass
		self.position += self.orientation * position
		self.velocity += self.orientation * velocity
		
		#assert sum((_i.velocity * _i.mass for _i in self), Vector.ZERO).within_cube(precision)
		
		# calculate angular corrections
		inertmoment = Matrix.ZERO
		angularmom  = Vector.ZERO
		
		for item in self.items:
			item_inertmom = self.orientation * item.orientation * Matrix.diag(item.inertmoment) * item.orientation.transpose() * self.orientation.transpose()
			item_position = self.orientation * item.position
			item_velocity = self.orientation * item.velocity + self.rotation % item_position
			item_rotation = self.rotation + item.rotation.cross_product_rotation(self.orientation)
			inertmoment  += item_inertmom + item.mass * (Matrix.UNIT * (item_position * item_position) - (item_position & item_position))
			angularmom   += item.mass * item_position % item_velocity + item_inertmom * item_rotation
		
		orientation, inertmoment, orient_t = inertmoment.orthonormal_eigendecomposition(precision)
		rotation  = orientation * Matrix.diag(inertmoment.power(-1)) * orient_t * angularmom
		
		# update item parameters
		for item in self.items:
			item.position     = self.orientation * item.position
			item.velocity     = self.orientation * item.velocity + self.rotation % item.position # item.position updated in the previous step
			item.orientation  = self.orientation * item.orientation
			item.rotation     = self.rotation + item.rotation.cross_product_rotation(self.orientation)
		
		# update system parameters
		self.inertmoment = inertmoment
		self.orientation = orientation
		self.rotation    = rotation
		
		for item in self.items:
			item.velocity     = orient_t * (item.velocity - self.rotation % item.position)
			item.position     = orient_t * item.position
			item.orientation  = orient_t * item.orientation
			item.rotation     = (item.rotation - self.rotation).cross_product_rotation(orient_t)
		
		#assert sum((_i.velocity * _i.mass for _i in self), Vector.ZERO).within_cube(precision)
		#try:
		#	corrections = self.corrections
		#except AttributeError:
		#	pass
		#else:
		#	for correction in corrections:
		#		correction()
		#
		#return self
	
	def push(self, *items, precision=1.e-12):
		"Push items into the container."
		
		if not items:
			return self
		
		for item in items:
			self.internal_frame(item, item)
			item.parent = self
			self.parent.items.remove(item)
		
		self.items.extend(items)
		self.correction(precision=precision)
		return self
	
	def pull(self, *items, precision=1.e-12):
		"Pull items out of the container."
		
		if not items:
			return self
		
		for item in items:
			self.external_frame(item, item)
			item.parent = self.parent
			self.items.remove(item)
		
		self.parent.items.extend(items)
		self.correction(precision=precision)
		return self
	
	def kinematics(self, dt, precision=1.e-12):
		for item in self:
			ext_item = Shadow(item)
			self.external_frame(item, ext_item)
			
			force = self.force(item, precision=precision)
			tide = self.orientation.transpose() * (self.parent.force(ext_item, precision=precision) - item.mass * self.acceleration)
			torque = self.torque(item, precision=precision)
			spin = self.orientation.transpose() * (self.parent.torque(ext_item, precision=precision) - item.inertmoment() * self.angularaccel)
			
			# rotational corrections
			centrifugal = self.rotation % (self.rotation % (self.orientation * item.position))
			coriolis = 2 * self.rotation % (self.orientation % item.velocity)
			euler = self.angularaccel % (self.orientation * item.position)
			precession = self.orientation.precession(self.rotation, item.rotation)
			
			# integrate
			item.acceleration = (force + tide) / item.mass - self.orientation.transpose() * (centrifugal + coriolis + euler)
			item.position += (item.velocity + item.acceleration * dt/2) * dt
			item.velocity += item.acceleration * dt
			item.angularaccel = item.orientation * Matrix.diag(item.inertmoment.power(-1)) * item.orientation.transpose() * (torque + spin) + precession
			item.orientation += Matrix.skew(item.rotation + item.angularaccel * dt/2) * item.orientation * dt
			item.rotation += item.angularaccel * dt


class Fluid(Object):
	def kinematics(self, dt, precision=1.e-12):
		for item in self:
			ext_item = Shadow(item)
			self.external_frame(item, ext_item)
			
			force = self.force(item, precision=precision)
			tide = self.orientation.transpose() * (self.parent.force(ext_item, precision=precision) - item.mass * self.acceleration)
			torque = self.torque(item, precision=precision)
			spin = self.orientation.transpose() * (self.parent.torque(ext_item, precision=precision) - item.inertmoment() * self.angularaccel)
			
			# rotational corrections
			centrifugal = self.rotation % (self.rotation % (self.orientation * item.position))
			coriolis = 2 * self.rotation % (self.orientation % item.velocity)
			euler = self.angularaccel % (self.orientation * item.position)
			precession = self.orientation.precession(self.rotation, item.rotation)
			
			item_xp = self.get_neighbour('+x')
			item_yp = self.get_neighbour('+y')
			item_zp = self.get_neighbour('+z')
			item_xn = self.get_neighbour('-x')
			item_yn = self.get_neighbour('-y')
			item_zn = self.get_neighbour('-z')
			
			flux_div = 0
			flux_div += Vector.X * (item.mass * item.velocity - item_xp.mass * item_xp.velocity) / (2 * abs(item.position - item_xp.position))
			flux_div -= Vector.X * (item.mass * item.velocity - item_xn.mass * item_xn.velocity) / (2 * abs(item.position - item_xn.position))
			flux_div += Vector.Y * (item.mass * item.velocity - item_yp.mass * item_yp.velocity) / (2 * abs(item.position - item_yp.position))
			flux_div -= Vector.Y * (item.mass * item.velocity - item_yn.mass * item_yn.velocity) / (2 * abs(item.position - item_yn.position))
			flux_div += Vector.Z * (item.mass * item.velocity - item_zp.mass * item_zp.velocity) / (2 * abs(item.position - item_zp.position))
			flux_div -= Vector.Z * (item.mass * item.velocity - item_zn.mass * item_zn.velocity) / (2 * abs(item.position - item_zn.position))
			
			item.acceleration = (force + tide) / item.mass - self.orientation.transpose() * (centrifugal + coriolis + euler)
			item.velocity += (item.accleration - item.velocity * flux_div) * dt
			item.mass -= flux_div * dt
			#item.angularaccel = ... * (torque + spin) + precession # FIXME
			#item.rotation += item.angularaccel * dt



class Universe(Object):
	"Universe: an object that has subitems but can not be a subitem itself. Universe is a root of object tree."
	
	def __init__(self, *args):
		self.items = []
		
		self.mass = 0
		
		self.interactions = []
		self.corrections = []
		self.forces = []
		self.torques = []
	
	def create(self, item, position=Vector.ZERO, velocity=Vector.ZERO, orientation=Matrix.UNIT, rotation=Vector.ZERO):
		self.items.append(item)
		self.mass += item.mass
		item.position = position
		item.velocity = velocity
		item.acceleration = Vector.ZERO
		item.rotation = rotation
		item.orientation = orientation
		item.angularaccel = Vector.ZERO
		item.parent = self
		if hasattr(item, 'interactions'):
			item.interactions.append(lambda prec: Universe.dynamics(item, prec))
			item.forces.append(lambda it, prec: Universe.force(item, it, prec))
			item.torques.append(lambda it, prec: Universe.torque(item, it, prec))
		return item
	
	def destroy(self, item):
		self.items.remove(item)
		self.mass -= item.mass
		del item.position
		del item.velocity
		del item.acceleration
		del item.orientation
		del item.rotation
		del item.angularaccel
		del item.parent
		return item
	
	def kinematics(self, dt, precision=1.e-12):
		for item in self:
			item.acceleration = self.force(item, precision=precision) / item.mass
			item.position += (item.velocity + item.acceleration * dt/2) * dt
			#item.position = Vector(*[Decimal(_c).quantize(precision) for _c in item.position])
			item.velocity += item.acceleration * dt
			
			item.angularaccel = item.orientation * Matrix.diag(item.inertmoment.power(-1)) * item.orientation.transpose() * self.torque(item, precision=precision)
			item.orientation += Matrix.skew(item.rotation + item.angularaccel * dt/2) * item.orientation * dt
			item.rotation += item.angularaccel * dt
	
	@staticmethod
	def interactions(Interactions):
		def init(self, *args, **kwargs):
			Universe.__init__(self, *args)
			for Interaction in Interactions:
				try:
					iargs = kwargs[Interaction.__name__]
				except KeyError:
					iargs = ()
				Interaction.__init__(self, *iargs)
		
		return type('Universe', (Universe,) + Interactions, {'__init__':init})


def physics_test():
	def random_orientation():
		oa = Vector(random.uniform(-5, 5), random.uniform(-5, 5), random.uniform(-5, 5)).vertor()
		ob = Vector(random.uniform(-5, 5), random.uniform(-5, 5), random.uniform(-5, 5)).vertor()
		oc = oa % ob
		ob = (oa + oa % (oc * random.uniform(0.5, 2)) + ob + ob % (oc * random.uniform(0.5, 2))).vertor()
		oa = ob % oc
		ok = Matrix(*([_x for _x in oa] + [_x for _x in ob] + [_x for _x in oc]))
		o1, _e, o2 = ok.singular_value_decomposition()
		o = o1 * o2
		return o
	
	def system_invariants_test(system, precision=1.e-12):
		assert sum([_i.position * _i.mass for _i in system], Vector.ZERO).within_cube(precision), "Center of mass nonzero: " + str(sum([_i.position * _i.mass for _i in system], Vector.ZERO))
		assert sum([_i.velocity * _i.mass for _i in system], Vector.ZERO).within_cube(precision), "Momentum nonzero: " + str(sum([_i.velocity * _i.mass for _i in system], Vector.ZERO))
		angular_momentum = Vector.ZERO
		for item in system:
			item_inertmom  = item.orientation * Matrix.diag(item.inertmoment) * item.orientation.transpose()
			angular_momentum += item_inertmom * item.rotation
			angular_momentum += (item.position % item.velocity) * item.mass
		assert angular_momentum.within_cube(100 * math.sqrt(precision)), "Angular momentum nonzero: " + str(angular_momentum)
	
	def system_sanity_test(system, precision=1.e-12):
		assert system.mass >= 0, "Mass is negative: " + str(system.mass)
		assert all(_i >= 0 for _i in system.inertmoment), "One of principal moments of inertia is negative: " + str(system.inertmoment)
		assert abs(system.orientation.det() - 1.0) < precision, "Orientation matrix is not positive definite."
		assert (system.orientation.inverse() - system.orientation.transpose()).small(math.sqrt(precision)), "Orientation matrix is not orthogonal: " + str(system.orientation)
	
	def system_push_test(n, sequential=False, precision=1.e-12):
		universe = Universe()
		system = universe.create(System())
		atoms = []
		
		mass = 0
		center = Vector.ZERO
		momentum = Vector.ZERO
		inertmoment = Matrix.ZERO
		angular_momentum = Vector.ZERO
		for i in range(n):
			m = random.uniform(0.00001, 10)
			p = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
			v = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
			i = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
			o = random_orientation()
			r = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
			atom = universe.create(Atom(m, i), position=p, velocity=v, orientation=o, rotation=r)
			
			mass += atom.mass
			center += atom.mass * atom.position
			momentum += atom.mass * atom.velocity
			item_inertmoment = atom.orientation * Matrix.diag(atom.inertmoment) * atom.orientation.transpose()
			orbital_inertmoment = atom.mass * (Matrix.UNIT * abs(atom.position)**2 - (atom.position & atom.position))
			angular_momentum += item_inertmoment * atom.rotation + atom.mass * (atom.position % atom.velocity)
			inertmoment += item_inertmoment + orbital_inertmoment
			
			atoms.append(atom)
			if sequential:
				system.push(atom, precision=precision)
		
		if not sequential:
			system.push(*atoms, precision=precision)
		system_sanity_test(system, precision=precision)
		assert len(system) == n, "Wrong number of items. Should be: " + str(n) + ". Got: " + len(system) + "."
		assert abs(system.mass - mass) < precision, "Mass of the system in not the sum of masses of its items."
		if mass > 0:
			assert (system.position - center / mass).within_cube(100 * precision), "Position of the system in not the center of mass of its items: " + str(system.position - center / mass)
			assert (system.velocity - momentum / mass).within_cube(100 * precision), "Velocity of the system in not the velocity of the center of mass of its items: " + str(system.velocity - momentum / mass)
			
			inertmoment -= mass * (Matrix.UNIT * abs(center / mass)**2 - ((center / mass) & center / mass))
			sys_inertmoment = system.orientation * Matrix.diag(system.inertmoment) * system.orientation.transpose()
			assert (sys_inertmoment - inertmoment).small(10 * mass * math.sqrt(precision)), "Moment of inertia of the system relative to its center of mass is invalid: " + str(sys_inertmoment - inertmoment)
			
			sys_angmom = system.mass * (system.position % system.velocity) + sys_inertmoment * system.rotation
			assert (sys_angmom - angular_momentum).within_cube(math.sqrt(precision)), "Angular momentum miscalculated: " + str(sys_angmom - angular_momentum)
		system_invariants_test(system, precision=precision)
	
	def system_single_item_test(precision=1.e-12):
		m = random.uniform(0.00001, 10)
		p = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		v = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		i = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
		o = random_orientation()
		r = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
		
		universe = Universe()
		atom = universe.create(Atom(m, i), position=p, velocity=v, orientation=o, rotation=r)
		system = universe.create(System())
		system.push(atom, precision=precision)
		system_sanity_test(system, precision=precision)
		assert abs(system.mass - m) < precision, "Wrong system mass"
		assert (system.position - p).within_cube(precision), "Wrong system position"
		assert (system.velocity - v).within_cube(precision), "Wrong system velocity"
		assert (system.orientation * Matrix.diag(system.inertmoment) * system.orientation.transpose() - o * Matrix.diag(i) * o.transpose()).small(math.sqrt(precision)), "Wrong system moment of inertia"
		assert (system.rotation - r).within_cube(math.sqrt(precision)), "Wrong system rotation: " + str(system.rotation) + " vs " + str(r)
		assert abs(atom.mass - m) < precision, "Wrong item mass"
		assert (atom.position).within_cube(precision), "Wrong item position (nonzero)"
		assert (atom.velocity).within_cube(precision), "Wrong item velocity (nonzero)"
		assert (atom.inertmoment - i).within_cube(precision), "Wrong item moment of inertia"
		assert (atom.rotation).within_cube(math.sqrt(precision)), "Wrong item rotation (nonzero): " + str(atom.rotation)
		system_invariants_test(system, precision=precision)
	
	def system_double_item_test(precision=1.e-12):
		m1 = random.uniform(0.00001, 10)
		p1 = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		v1 = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		i1 = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
		o1 = random_orientation()
		r1 = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
		
		m2 = random.uniform(0.00001, 10)
		p2 = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		v2 = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
		i2 = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
		o2 = random_orientation()
		r2 = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
		
		universe = Universe()
		atom1 = universe.create(Atom(m1, i1), position=p1, velocity=v1, orientation=o1, rotation=r1)
		atom2 = universe.create(Atom(m2, i2), position=p2, velocity=v2, orientation=o2, rotation=r2)
		system = universe.create(System())
		
		#system.push(atom1, atom2, precision=precision)
		system.push(atom1, precision=precision)
		system.push(atom2, precision=precision)
		
		system_sanity_test(system, precision=precision)
		assert (system.position - (m1 * p1 + m2 * p2) / (m1 + m2)).within_cube(precision), "Center of mass miscalculated by: " + str(abs(system.position - (m1 * p1 + m2 * p2) / (m1 + m2)))
		assert (system.velocity - (m1 * v1 + m2 * v2) / (m1 + m2)).within_cube(precision), "Total momentum miscalculated."
		assert (system.position + system.orientation * atom1.position - p1).within_cube(precision), "1st item position miscalculated."
		assert (system.position + system.orientation * atom2.position - p2).within_cube(precision), "2nd item position miscalculated."
		system_invariants_test(system, precision=precision)
	
	def system_sequential_test(n=32, precision=1.e-12):
		for i in range(n):
			system_push_test(i, sequential=True, precision=precision)
	
	def system_nonsequential_test(n=32, precision=1.e-12):
		for i in range(n):
			system_push_test(i, sequential=False, precision=precision)
	
	def system_push_pull_test(n=32, precision=1.e-12):
		universe = Universe()
		system = universe.create(System())
		atoms = []
		for nn in range(n):
			m = random.uniform(0.00001, 10)
			p = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
			v = Vector(random.gauss(0, 5), random.gauss(0, 5), random.gauss(0, 5))
			i = Vector(random.uniform(0.00001, 3), random.uniform(0.00001, 3), random.uniform(0.00001, 3))
			o = random_orientation()
			r = Vector(random.gauss(0, 3), random.gauss(0, 3), random.gauss(0, 3))
			atom = universe.create(Atom(m, i), position=p, velocity=v, orientation=o, rotation=r)
			atoms.append(atom)
		system.push(*atoms, precision=precision)
		system_sanity_test(system, precision=precision)
		system_invariants_test(system, precision=precision)
		m = system.mass
		p = system.position
		v = system.velocity
		i = system.inertmoment
		o = system.orientation
		r = system.rotation
		for nn in range(n):
			atom = random.choice(atoms)
			system.pull(atom, precision=precision)
			system_sanity_test(system, precision=precision)
			system_invariants_test(system, precision=precision)
			system.push(atom, precision=precision)
			system_sanity_test(system, precision=precision)
			system_invariants_test(system, precision=precision)
			assert abs(system.mass - m) < precision
			assert (system.position - p).within_cube(precision)
			assert (system.velocity - v).within_cube(precision)
			assert (system.orientation * Matrix.diag(system.inertmoment) * system.orientation.transpose() - o * Matrix.diag(i) * o.transpose()).small(math.sqrt(precision))
			# FIXME: assert (system.orientation - o).small(math.sqrt(precision)), "error: " + str(system.orientation * o.transpose())
			assert (system.rotation - r).within_cube(precision), "error: " + str(system.rotation - r)
	
	"Begin tests"
	assert True, "Because f*ck you, that's why."
	for i in range(32):
		system_push_pull_test(32, precision=1.e-12)
	for i in range(32):
		system_single_item_test(precision=1.e-12)
	for i in range(32):
		system_double_item_test(precision=1.e-12)
	for i in range(8):
		system_sequential_test(16, precision=1.e-12)
	for i in range(8):
		system_nonsequential_test(16, precision=1.e-12)
	



'''
X, V, A, M
Q, R, T, I

∂X = V ∂t
∂V = A ∂t

∂Q = Rm Q ∂t
∂R = T ∂t

     [ 0   Rz -Ry]
Rm = [-Rz  0   Rx]
     [ Ry -Rx  0 ]

    [Rx]
R = [Ry]
    [Rz]


Q' = (1 + Rm ∂t) Q
Q' = Q + Rm Q ∂t
Q' - Q = Rm Q ∂t
∂Q = Rm Q ∂t


M_ext[i] = M_int[i]
I_ext[i] = Q_sys I_int[i] Q_sys^(-1)

X_ext[i] = X_sys + Q_sys X_int[i]
V_ext[i] = V_sys + Q_sys V_int[i] + R_sys × Q_sys X_int[i]
A_ext[i] = A_sys + Q_sys A_int[i] + 2 R_sys × Q_sys V_int[i] + T_sys × Q_sys V_int[i] + R_sys × (R_sys × Q_sys X_int[i])
Q_ext[i] = Q_sys Q_int[i]
R_ext[i] = R_sys + rotvect(Q_sys skew(R_int[i]) Q_sys^T)
T_ext[i] = T_sys + rotvect(Q_sys skew(T_int[i]) Q_sys^T) + rotvect(skew(R_sys) Q_sys skew(R_int[i]) Q_sys^T - (skew(R_sys) Q_sys skew(R_int[i]) Q_sys^T)^T)


A_int[i] = F_int[i] / M[i] - Q_sys^T (2 R_sys × Q_sys V_int[i] + T_sys × Q_sys V_int[i] + R_sys × (R_sys × Q_sys X_int[i]))
F_ext[i] = Q_sys F_int[i] + M[i] A_sys
F_int[i] = Q_sys^T (F_ext[i] - M[i] A_sys)

T_int[i] = Q_sys^T (T_ext[i] - T_sys) Q_sys - (Q_sys^T R_sys Q_sys R_int[i] - R_int[i] Q_sys^T R_sys Q_sys)
Tr_int[i] = Q_sys^T (T_ext[i] - T_sys) Q_sys


∑ M_int[i] X_int[i] = 0
∑ M_int[i] V_int[i] = 0
∑ M_int[i] A_int[i] = 0
∑ (M_int[i] X_int[i] × V_int[i] + I_int[i] R_int[i]) = 0
∑ (M_int[i] X_int[i] × A_int[i] + I_int[i] T_int[i] + ∂I_int[i] R_int[i]) = 0

I = Q diag(Iv) Q^T
∂I = ∂(Q diag(Iv) Q^T) =
   = ∂Q diag(Iv) Q^T + Q ∂diag(Iv) Q^T + Q diag(Iv) ∂Q^T =
   = Rm Q ∂t diag(Iv) Q^T + Q diag(∂Iv) Q^T + Q diag(Iv) Q^T (-Rm) ∂t = 
   = (Rm I - I Rm) ∂t + Q diag(Dv) Q^T =
   = (Rm I - I Rm) ∂t + D



I_sys = ∑ (I_ext[i] + M_ext[i] skew(X_ext[i] - X_sys)^2)
∂I_sys = ∂∑ (I_ext[i] + M_ext[i] skew(X_ext[i] - X_sys)^2) =
       = ∑ (∂I_ext[i]) + ∑ ∂(M_ext[i] skew(X_ext[i] - X_sys)^2) =
       = ∑ (∂I_ext[i]) + ∑ (M_ext[i] ∂skew(X_ext[i] - X_sys)^2) =


M_sys = ∑ M_ext[i]
I_sys = ∑ (I_ext[i] + M_ext[i] skew(X_ext[i] - X_sys)^2)
X_sys = ∑ M_ext[i] X_ext[i] / M_sys
V_sys = ∑ M_ext[i] V_ext[i] / M_sys
A_sys = ∑ M_ext[i] A_ext[i] / M_sys

//R_sys = I_sys^{-1} ∑ (I_ext[i] R_ext[i] + M_ext[i] X_ext[i] × V_ext[i])
//T_sys = I_sys^{-1} ∑ (M_ext[i] X_ext[i] × A_ext[i] + I_ext[i] T_ext[i] + ∂I_ext[i] R_ext[i])



L_sys = ∑ (I_ext[i] R + M_ext[i] (X_ext[i] - X_sys) × R × (X_ext[i] - X_sys))
L_sys = ∑ (I_ext[i] R_ext[i] + M_ext[i] X_ext[i] × V_ext[i]) - M_sys X_sys × V_sys






A_ext[i] = A_sys + Q_sys A_int[i] + R_sys × R_sys × Q_sys X_int[i] + 2 R_sys × Q_sys V_int[i] + T_sys × Q_sys X_int[i]
A_int[i] = Q_sys^-1 (A_ext[i] - A_sys - R_sys × R_sys × Q_sys X_int[i] - 2 R_sys × Q_sys V_int[i] - T_sys × Q_sys X_int[i])
A_int[i] = F_int[i] / M[i] - Q_sys^-1 (A_sys - R_sys × R_sys × Q_sys X_int[i] - 2 R_sys × Q_sys V_int[i] - T_sys × Q_sys X_int[i])




A_ext[i] = A_sys + Q_sys A_int[i] + 2 R_sys × Q_sys V_int[i] + T_sys × Q_sys V_int[i] + R_sys × (R_sys × Q_sys X_int[i])




Z = M_sys X_sys
P = M_sys V_sys
F = M_sys A_sys

L = I_sys R_sys
C = I_sys T_sys

X_ab = ((M_a X_a) + (M_b X_b)) / (M_a + M_b)
X_b = (X_ab (M_a + M_b) - (M_a X_a)) / M_b


'''




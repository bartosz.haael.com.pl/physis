#!/usr/bin/python3
#-*- coding:utf-8 -*-


from __future__ import unicode_literals

import random
import math

from .helpers import *
from .algebra import *
from .physics import Object


class Fluid(Object):
	@staticmethod
	def smoothing_kernel(distance, scale, precision=1.e-12):
		"Kernel of the smoothing function."
		return (scale - distance) if (distance < scale) else 0
	
	@staticmethod
	def smoothing_kernel_support(distance, scale, precision=1.e-12):
		"Return True if the kernel function is not zero."
		return distance < scale
	
	@staticmethod
	def smoothing_kernel_integral(scale, precision=1.e-12):
		"Spatial integral of the kernel function."
		return 4/3 * math.pi * scale ** 3
	
	@staticmethod
	def smoothing_kernel_derivative(distance, scale, precision=1.e-12):
		"Spatial derivative of the kernel function."
		return -1 if (precision < distance < scale) else 0
	
	def smoothing_scale(self, item, precision=1.e-12):
		return 0
	
	def smoothed_function(self, fun, origin, precision=1.e-12):
		result = 0
		for item in self.__quantization:
			distance = abs(item.position - origin)
			scale = self.smoothing_scale(item, precision)
			if self.smoothing_kernel_support(distance, scale, precision):
				result += fun(item) * self.smoothing_kernel_integral(scale, precision) * self.smoothing_kernel(distance, scale, precision)
		return result
	
	def smoothed_gradient(self, fun, origin, precision=1.e-12):
		result = Vector.ZERO
		for item in self.__quantization:
			ray = item.position - origin
			distance = abs(ray)
			scale = self.smoothing_scale(item, precision)
			if self.smoothing_kernel_support(distance, scale, precision):
				result += fun(item) * self.smoothing_kernel_integral(scale, precision) * self.smoothing_kernel_derivative(distance, scale, precision) * ray.vertor()
		return result
	
	def split_atom(self, atom, particles):
		#self.items.remove(atom)
		
		new_mass = atom.mass / particles
		
		atoms = []
		for i in range(particles):
			new_atom = Shadow(atom)
			new_atom.mass = new_mass
			new_atom.position += random() # FIXME
			atoms.append(new_atom)
		
		helper_system = System()
		helper_system.parent = self
		helper_system.push(*atoms)
		
		new_inertmoment = (helper_system.inertmoment + atom.inertmoment) / particles - atom.inertmoment
		
		helper_system.parent = self
		helper_system.correct()
		helper_system.position = atom.position
		helper_system.pull(*atoms)
	
	def join_atoms(self, *atoms):
		helper_system = System()
		helper_system.parent = self
		helper_system.push(*atoms)
		atom = Atom(helper_system.mass, helper_system.inertmoment)
		self.items.append(atom)
		return atom





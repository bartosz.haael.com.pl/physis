#!/usr/bin/python3
#-*- coding:utf-8 -*-


from __future__ import unicode_literals

import random
import math

from .helpers import *
from .algebra import *
from .physics import Object, Shadow



class Gravity(Object):
	def __init__(self, g):
		self.gravitation_field = []
		self.gravitation_constant = g
		
		#self.interactions.append(self.__dynamics)
		#self.forces.append(self.__force)
		#self.torques.append(self.__torque)
	
	def dynamics(self, precision=1.e-12):
		self.gravitation_field = []
		for item in self:
			shadow = Shadow(item)
			shadow.position = item.position
			shadow.mass = item.mass
			self.gravitation_field.append(shadow)
	
	def force(self, item, precision=1.e-12):
		gravitational_field_strength = Vector.ZERO
		for source in self.gravitation_field:
			if source == item:
				continue
			displacement = item.position - source.position
			if abs(displacement) > precision:
				gravitational_field_strength -= displacement.vertor() * (mass / abs(displacement)**2)
		return self.gravitation_constant * item.mass * gravitational_field_strength
	
	def torque(self, item, precision=1.e-12):
		raise NotImplementedError


'''
Object.electric_charge = 0
Object.electric_dipole = Vector.ZERO
Object.magnetic_dipole = Vector.ZERO

class Electromagnetism(Object):
	def __init__(self, e, m):
		self.electromagnetic_field = []
		self.electric_constant = e
		self.magnetic_constant = m
		
		self.interactions.append(self.__dynamics)
		self.forces.append(self.__force)
		#self.torques.append(self.__torque)
		self.corrections.append(self.__correct)
	
	def __correct(self, precision=1.e-12):
		electric_charge = 0
		electric_dipole = Vector.ZERO
		magnetic_dipole = Vector.ZERO
		for item in self:
			electric_charge += item.electric_charge
			electric_dipole += item.electric_charge * item.position + item.orientation * item.electric_dipole
			magnetic_dipole += item.electric_charge * item.position % item.velocity + item.orientation * item.magnetic_dipole
		self.electric_charge = electric_charge
		self.electric_dipole = electric_dipole
	
	def __dynamics(self, precision=1.e-12):
		self.electromagnetic_field = []
		for item in self:
			shadow = Shadow(item)
			shadow.position = item.position
			shadow.velocity = item.velocity
			shadow.electric_charge = item.electric_charge
			shadow.electric_dipole = item.electric_dipole
			self.electromagnetic_field.append(shadow)
	
	def __force(self, item, precision=1.e-12):
		electric_field_strength = Vector.ZERO
		magnetic_field_strength = Vector.ZERO
		for source in self.electromagnetic_field:
			displacement = position - item.position
			if abs(displacement) > precision:
				charge, velocity = params
				strength = displacement.vertor() * (charge / abs(displacement)**2)
				movement = velocity - item.velocity
				electric_field_strength += strength
				magnetic_field_strength += movement % strength
		return item.electric_charge * (self.electric_constant * electric_field_strength + self.magnetic_constant * magnetic_field_strength)
	
	def __torque(self, item, precision=1.e-12): #FIXME
		electric_field_strength = Vector.ZERO
		magnetic_field_strength = Vector.ZERO
		for position, params in self.electromagnetic_field.items():
			displacement = position - item.position
			if abs(displacement) > precision:
				charge, velocity = params
				strength = displacement.vertor() * (charge / abs(displacement)**2)
				movement = velocity - item.velocity
				electric_field_strength += strength
				magnetic_field_strength += movement % strength
		return item.electric_dipole % (self.electric_constant * electric_field_strength + self.magnetic_constant * magnetic_field_strength)
'''



Object.stress = Matrix.ZERO

class Pressure(Object):
	def __init__(self, gas_constant):
		pass
	
	def __pressure_gradient(self, origin):
		return self.smoothed_gradient(lambda _item: _item.pressure, origin)
	
	def __force(self, item, precision=1.e-12):
		return -self.__pressure_gradient(item.position)


class Viscosity(Object):
	pass


class Stress(Object):
	pass




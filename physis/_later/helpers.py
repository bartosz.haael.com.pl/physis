#!/usr/bin/python3
#-*- coding:utf-8 -*-


from __future__ import unicode_literals


__all__ = ["Record", "Shadow", "static_property"]


class Record(object):
	def __init__(self, **args):
		for name, value in args.iteritems():
			if name[0] == "_": raise ValueError("Cannot assign attributes starting with '_' via constructor.")
			setattr(self, name, value)


class Shadow(Record):
	def __init__(self, base, **args):
		Record.__init__(self, **args)
		self.__base = base
	
	def __eq__(self, other):
		if isinstance(other, Shadow):
			return self.__base == other.__base
		else:
			return self.__base == other
	
	def __getattr__(self, attr):
		return getattr(self.__base, attr)


class StaticProperty(object):
	def __init__(self, method):
		self.__method = method
	
	def __get__(self, instance, owner):
		return self.__method()
	
	def __set__(self, instance, value):
		raise AttributeError()
	
	def __delete__(self, instance):
		raise AttributeError()


def static_property(method):
	return StaticProperty(method)




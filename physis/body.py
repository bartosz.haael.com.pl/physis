#!/usr/bin/python3
#-*- coding:utf-8 -*-



import random
import math

from .helpers import *
from .algebra import *



__all__ = []



#Object.mass        = 0
#Object.inertmoment = Vector.ZERO
#Object.deformation = Vector.ZERO


class Body(Object):
	def __init__(self, substance, amount):
		self.substance = substance
		self.amount = amount
	
	def density(self, point, radius):
		return 0


class Point(Body):
	"Atom: an object that has no sub-items. Atoms are leaves of object tree."
	
	pass


class Ball(Body):
	def __init__(self, radius, **kwargs):
		self.radius = radius
		super().__init__(**kwargs)


class Hollow(Body):
	def __init__(self, inner_radius, outer_radius, **kwargs):
		pass


class Cloud(Body):
	def __init__(self, deviation, **kwargs):
		self.deviation = deviation
		super().__init__(**kwargs)


class Polyhedron(Body):
	def __init__(self, shape, **kwargs):
		self.shape = shape
		super().__init__(**kwargs)














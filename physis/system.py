#!/usr/bin/python3
#-*- coding:utf-8 -*-


import random
import math

from .algebra import *
from .core import *


__all__ = ['System', 'Sparse', 'Dense']


Object.deformation  = Vector.ZERO

Object.position     = Vector.ZERO
Object.velocity     = Vector.ZERO
Object.acceleration = Vector.ZERO

Object.orientation = Matrix.UNIT
Object.rotation    = Vector.ZERO
Object.accelmoment = Vector.ZERO


class System(Object):
	"System: an object that can be a subitem and can have subitems. Systems are internal nodes of object tree."
	
	def __init__(self):
		self.items = []
		
		self.mass        = 0
		self.inertmoment = Vector.ZERO
		self.deformation = Vector.ZERO
	
	def __iter__(self):
		yield from self.items
	
	def __contains__(self, item):
		return item in self.items
	
	def assert_equations(self, precision=precision):
		mass        = 0
		inertia     = Matrix.ZERO
		deformation = Matrix.ZERO
		
		center   = Vector.ZERO
		momentum = Vector.ZERO
		force    = Vector.ZERO
		angular  = Vector.ZERO
		torque   = Vector.ZERO
		
		def COMMUTATOR(a, b):
			return a * b - b * a
		
		def ANTICOMMUTATOR(a, b):
			return a * b + b * a
		
		for item in self:
			mass        += item.mass # mass
			
			center      += item.mass * item.position # center of mass
			
			momentum    += item.mass * item.velocity # momentum
			
			force       += item.mass * item.acceleration # force
			
			tensor_inertmoment = item.orientation * Matrix.diag(item.inertmoment) * item.orientation.transpose()
			inertia     += tensor_inertmoment # intrinsic moment of inertia
			inertia     += item.mass * Matrix.skew(item.position).square() # orbital moment of inertia
			
			# FIXME: correct?
			tensor_deformation = item.orientation * Matrix.diag(item.deformation) * item.orientation.transpose()
			deformation += tensor_deformation
			deformation += COMMUTATOR(Matrix.skew(item.rotation), tensor_inertmoment)
			deformation += item.mass * ANTICOMMUTATOR(Matrix.skew(item.position), Matrix.skew(item.velocity))
			
			angular     += item.mass * item.position % item.velocity # intrinsic angular momentum
			angular     += tensor_inertmoment * item.rotation # orbital angular momentum
			
			# FIXME: correct?
			torque      += item.mass * item.position % item.acceleration # intrinsic torque
			torque      += tensor_inertmoment * item.angularaccel # orbital torque
			torque      += tensor_deformation * item.rotation # deformation torque
		
		assert abs(mass - self.mass) < precision, "mass must be sum of system's parts"
		#assert (inertia - self.inertia).small(precision), "invalid moment of inertia" # TODO: assertion
		#assert deformation # TODO: assertion
		
		assert abs(center)   < precision, "center of mass must lie in the point zero"
		assert abs(momentum) < precision, "net momentum must be zero"
		assert abs(force)    < precision, "net force must be zero"
		assert abs(angular)  < precision, "net angular momentum must be zero"
		assert abs(torque)   < precision, "net torque must be zero"
	
	def external_frame(self, srcitem, dstitem):
		"Change item parameters to the external frame. `srcitem` and `dstitem` may be the same object."
		
		ext_pos = self.orientation * srcitem.position
		
		position     = self.position + ext_pos
		velocity     = self.velocity + self.orientation * srcitem.velocity + self.rotation % ext_pos
		acceleration = self.acceleration + self.orientation * srcitem.acceleration + self.rotation % (self.rotation % (self.orientation * srcitem.position)) + 2 * self.rotation % (self.orientation * srcitem.velocity) + self.accelmoment % ext_pos
		orientation  = self.orientation * srcitem.orientation
		rotation     = self.rotation + srcitem.rotation.cross_product_rotation(self.orientation)
		angularaccel = self.angularaccel + srcitem.angularaccel.cross_product_rotation(self.orientation) + self.orientation.precession(self.rotation, srcitem.rotation)
		
		dstitem.position     = position
		dstitem.velocity     = velocity
		dstitem.acceleration = acceleration
		dstitem.orientation  = orientation
		dstitem.rotation     = rotation
		dstitem.angularaccel = angularaccel
	
	def internal_frame(self, srcitem, dstitem):
		"Change item parameters to the internal frame. `srcitem` and `dstitem` may be the same object."
		
		frame_orient_t = self.orientation.transpose()
		ext_pos = srcitem.position - self.position
		ext_vel = srcitem.velocity - self.rotation % ext_pos - self.velocity
		
		position     = frame_orient_t * ext_pos
		velocity     = frame_orient_t * ext_vel
		acceleration = frame_orient_t * (srcitem.acceleration - self.rotation % (self.rotation % ext_pos) - 2 * self.rotation % ext_vel - self.accelmoment % ext_pos - self.acceleration)
		orientation  = frame_orient_t * srcitem.orientation
		rotation     = (srcitem.rotation - self.rotation).cross_product_rotation(self.orientation.transpose())
		angularaccel = (srcitem.angularaccel - self.angularaccel - self.orientation.precession(self.rotation, rotation)).cross_product_rotation(self.orientation.transpose())
		
		dstitem.position     = position
		dstitem.velocity     = velocity
		dstitem.acceleration = acceleration
		dstitem.orientation  = orientation
		dstitem.rotation     = rotation
		dstitem.angularaccel = angularaccel


class Sparse(System):
	def correction(self, precision=precision): #TODO: optimize this method
		# calculate linear corrections
		mass     = 0
		center   = Vector.ZERO
		momentum = Vector.ZERO
		
		for item in self.items:
			mass     += item.mass
			center   += item.mass * item.position
			item.velocity += self.orientation.transpose() * (self.rotation % (self.orientation * item.position))
			momentum += item.mass * item.velocity
		
		position = center / mass
		velocity = momentum / mass
		
		# update item parameters
		for item in self.items:
			item.position -= position
			item.velocity -= velocity + self.orientation.transpose() * (self.rotation % (self.orientation * item.position))
		
		# update system parameters
		self.mass     = mass
		self.position += self.orientation * position
		self.velocity += self.orientation * velocity
		
		#assert sum((_i.velocity * _i.mass for _i in self), Vector.ZERO).within_cube(precision)
		
		# calculate angular corrections
		inertmoment = Matrix.ZERO
		angularmom  = Vector.ZERO
		
		for item in self.items:
			item_inertmom = self.orientation * item.orientation * Matrix.diag(item.inertmoment) * item.orientation.transpose() * self.orientation.transpose()
			item_position = self.orientation * item.position
			item_velocity = self.orientation * item.velocity + self.rotation % item_position
			item_rotation = self.rotation + item.rotation.cross_product_rotation(self.orientation)
			inertmoment  += item_inertmom + item.mass * (Matrix.UNIT * (item_position * item_position) - (item_position & item_position))
			angularmom   += item.mass * item_position % item_velocity + item_inertmom * item_rotation
		
		orientation, inertmoment, orient_t = inertmoment.orthonormal_eigendecomposition(precision)
		rotation  = orientation * Matrix.diag(inertmoment.power(-1)) * orient_t * angularmom
		
		# update item parameters
		for item in self.items:
			item.position     = self.orientation * item.position
			item.velocity     = self.orientation * item.velocity + self.rotation % item.position # item.position updated in the previous step
			item.orientation  = self.orientation * item.orientation
			item.rotation     = self.rotation + item.rotation.cross_product_rotation(self.orientation)
		
		# update system parameters
		self.inertmoment = inertmoment
		self.orientation = orientation
		self.rotation    = rotation
		
		for item in self.items:
			item.velocity     = orient_t * (item.velocity - self.rotation % item.position)
			item.position     = orient_t * item.position
			item.orientation  = orient_t * item.orientation
			item.rotation     = (item.rotation - self.rotation).cross_product_rotation(orient_t)
		
		#assert sum((_i.velocity * _i.mass for _i in self), Vector.ZERO).within_cube(precision)
		#try:
		#	corrections = self.corrections
		#except AttributeError:
		#	pass
		#else:
		#	for correction in corrections:
		#		correction()
		#
		#return self
	
	def push(self, *items, precision=precision):
		"Push items into the container."
		
		if not items:
			return self
		
		for item in items:
			self.parent.items.remove(item)
			self.internal_frame(item, item)
			item.parent = self
		
		self.items.extend(items)
		self.correction(precision=precision)
		return self
	
	def pull(self, *items, precision=precision):
		"Pull items out of the container."
		
		if not items:
			return self
		
		#missing = [_i for _i in items if _i not in self]
		#for item in self:
		#	item.
		
		for item in items:
			self.items.remove(item)
			self.external_frame(item, item)
			item.parent = self.parent
		
		self.parent.items.extend(items)
		self.correction(precision=precision)
		return self
	
	def kinematics(self, dt, precision=precision):
		for item in self:
			inv_inertia = item.orientation.transpose() * Matrix.diagonal(item.inertmoment.power(-1)) * item.orientation
			ext_item = item.shadow()
			
			# interactions
			force = self.force(item)
			tide = self.parent.force(ext_item)
			torque = self.torque(item)
			moment = self.parent.torque(ext_item)
			
			# rotational corrections
			centrifugal = self.rotation % (self.rotation % (self.orientation * item.position))
			coriolis = 2 * self.rotation % (self.orientation % item.velocity)
			euler = self.angularaccel % (self.orientation * item.position)
			precession = self.orientation.precession(self.rotation, item.rotation)
			
			# integrate
			item.acceleration = force / item.mass + self.orientation.transpose() * (tide / item.mass - self.acceleration - centrifugal - coriolis - euler)
			item.position += (item.velocity + item.acceleration * dt/2) * dt
			item.velocity += item.acceleration * dt
			item.angularaccel = inv_inertia * torque + TRANSFORM(inv_inertia * moment - self.angularaccel) + precession # FIXME
			item.orientation += Matrix.skew(item.rotation + item.angularaccel * dt/2) * item.orientation * dt
			item.rotation += item.angularaccel * dt
	
	def tick(self, dt, precision=precision):
		pass


class Dense(System):
	def kinematics(self, dt, precision=precision):
		for item in self:
			ext_item = Shadow(item)
			self.external_frame(item, ext_item)
			
			force = self.force(item, precision=precision)
			tide = self.orientation.transpose() * (self.parent.force(ext_item, precision=precision) - item.mass * self.acceleration)
			torque = self.torque(item, precision=precision)
			spin = self.orientation.transpose() * (self.parent.torque(ext_item, precision=precision) - item.inertmoment() * self.angularaccel)
			
			# rotational corrections
			centrifugal = self.rotation % (self.rotation % (self.orientation * item.position))
			coriolis = 2 * self.rotation % (self.orientation % item.velocity)
			euler = self.angularaccel % (self.orientation * item.position)
			precession = self.orientation.precession(self.rotation, item.rotation)
			
			item_xp = self.get_neighbour('+x')
			item_yp = self.get_neighbour('+y')
			item_zp = self.get_neighbour('+z')
			item_xn = self.get_neighbour('-x')
			item_yn = self.get_neighbour('-y')
			item_zn = self.get_neighbour('-z')
			
			flux_div = 0
			flux_div += Vector.X * (item.mass * item.velocity - item_xp.mass * item_xp.velocity) / (2 * abs(item.position - item_xp.position))
			flux_div -= Vector.X * (item.mass * item.velocity - item_xn.mass * item_xn.velocity) / (2 * abs(item.position - item_xn.position))
			flux_div += Vector.Y * (item.mass * item.velocity - item_yp.mass * item_yp.velocity) / (2 * abs(item.position - item_yp.position))
			flux_div -= Vector.Y * (item.mass * item.velocity - item_yn.mass * item_yn.velocity) / (2 * abs(item.position - item_yn.position))
			flux_div += Vector.Z * (item.mass * item.velocity - item_zp.mass * item_zp.velocity) / (2 * abs(item.position - item_zp.position))
			flux_div -= Vector.Z * (item.mass * item.velocity - item_zn.mass * item_zn.velocity) / (2 * abs(item.position - item_zn.position))
			
			item.acceleration = (force + tide) / item.mass - self.orientation.transpose() * (centrifugal + coriolis + euler)
			item.velocity += (item.accleration - item.velocity * flux_div) * dt
			item.mass -= flux_div * dt
			#item.angularaccel = ... * (torque + spin) + precession # FIXME
			#item.rotation += item.angularaccel * dt




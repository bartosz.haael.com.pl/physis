#!/usr/bin/python3
#-*- coding:utf-8 -*-


from .algebra import *


__all__ = ['Meta', 'Object', 'precision']


precision = 1.e-12


class Meta(type):
	class Hook:
		@classmethod
		def subsymbols(cls, name):
			Root = cls.__mro__[0]
			original = object.__getattribute__(Root, name)
			
			symbols = []
			for Class in cls.__mro__[1:]:
				if issubclass(Root, Class):
					continue
				
				try:
					symbol = object.__getattribute__(Class, name)
				except AttributeError:
					continue
				
				if (symbol is not original) and (symbol not in symbols):
					symbols.append(symbol)
			return symbols
		
		def delegate(self, name, *args, **kwargs):
			return self.subsymbols(name)[0](self, *args, **kwargs)
	
	def __new__(cls, name, bases, clsdict):
		if any(hasattr(_cls, '__meta_root') for _cls in bases):
			new = super().__new__(cls, name, bases, clsdict)
			new.__meta_root = False
			return new
		else:
			new = super().__new__(cls, name, bases + (cls.Hook,), clsdict)
			new.__meta_root = True
			return new
	
	def mro(cls):
		mro = []
		for Class in super().mro():
			try:
				if Class.__meta_root:
					mro.insert(0, Class)
				else:
					mro.append(Class)
			except AttributeError:
				mro.append(Class)
		return mro


class Object(metaclass=Meta):
	mass        = 0
	inertmoment = Vector.ZERO
	
	def __init__(self, *args, **kwargs):
		self.delegate('__init__', *args, **kwargs)
	
	def copy(self):
		new = Object.__new__(self.__class__)
		for name, value in self.__dict__.items():
			setattr(new, name, value)
		return new
	
	def shadow(self):
		new = Object.__new__(self.__class__)
		new.__shadow = self
		return new
	
	def commit(self):
		shadow = self.__shadow
		for name, value in self.__dict__.items():
			setattr(shadow, name, value)
	
	def __getattr__(self, attr):
		if attr == '_Object__shadow':
			raise AttributeError("This object is not a shadow.")
		
		try:
			shadow = self.__shadow
		except AttributeError:
			raise AttributeError("Attribute `" + attr + "` not found.")
		
		return getattr(self.__shadow, attr)
	
	def assert_equations(self, precision=precision):
		for submethod in self.subsymbols('assert_equations'):
			submethod(self, precision=precision)
	
	def correction(self, precision=precision):
		for submethod in self.subsymbols('correction'):
			submethod(self, precision=precision)
	
	def external_frame(self, srcitem, dstitem):
		target = dstitem.shadow()
		for submethod in self.subsymbols('external_frame'):
			submethod(self, srcitem, target)
		target.commit()
	
	def internal_frame(self, srcitem, dstitem):
		target = dstitem.shadow()
		for submethod in self.subsymbols('internal_frame'):
			submethod(self, srcitem, target)
		target.commit()
	
	def force(self, item, precision=precision):
		force = Vector.ZERO
		for submethod in self.subsymbols('force'):
			force += submethod(self, item, precision=precision)
		return force
	
	def torque(self, item, precision=precision):
		torque = Vector.ZERO
		for submethod in self.subsymbols('torque'):
			torque += submethod(self, item, precision=precision)
		return torque
	
	def dynamics(self, precision=precision):
		for submethod in self.subsymbols('dynamics'):
			submethod(self, precision=precision)
	
	def kinematics(self, dt, precision=precision):
		for submethod in self.subsymbols('kinematics'):
			submethod(self, dt, precision=precision)
	
	def tick(self, dt, precision=precision):
		self.dynamics(precision=precision)
		self.kinematics(dt, precision=precision)
		try:
			for item in self:
				item.tick(dt, precision=precision)
		except TypeError:
			pass
		self.delegate('tick', dt, precision=precision)
		
		try:
			self.assert_equations(precision=precision)
		except AssertionError:
			self.correction(precision=precision)




'''

Universe > System > Body

cosmos = Cosmos()
cosmos.create(Sun())
cosmos.create(Planet(), position=Vector(1000, 0, 0), velocity=Vector(0, 10, 0))



a = Solid()
b, c = a.split()


system.push(thing)
thing.rasterize(system)
system.tick()
thing.vectorize(system)



item_list = self.items[:]
while item_list:
	itemA = item_list.pop()
	for itemB in item_list:
		if itemA.radius + itemB.radius > abs(itemA.position - itemB.position):
			bag = System()
			self.items.append(bag)
			bag.parent = self
			bag.push(itemA, itemB)
			item_list.remove(itemB)
			itemA.pull(*[_item for _item in itemA])
			itemB.pull(*[_item for _item in itemB])
			self.items.remove(itemA)
			self.items.remove(itemB)

for item in self.items[:]:
	if item.mass == 0:
		self.items.remove(item)
	elif item.mass == self.mass:
		self.pull(item)

sections = {}
r = self.radius / 3
for item in self.items[:]:
	if abs(item.position - origin) < r:
		section.push(item)



'''



#!/usr/bin/python3
#-*- coding: utf-8 -*-


from .svd import svd

import math


__all__ = ["Vector", "Matrix"]


class Vector:
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z
	
	#@staticmethod
	#def copy(other):
	#	"Copy a vector and return the new instance."
	#	return Vector(other.x, other.y, other.z)
	
	def __hash__(self):
		return hash((0xa8521082, self.x, self.y, self.z))
	
	def __len__(self):
		"Dimension of the vector space. Equals 3."
		return 3
	
	def __getitem__(self, n):
		if isinstance(n, int):
			if   n == 0: return self.x
			elif n == 1: return self.y
			elif n == 2: return self.z
			else: raise IndexError("Numeric index not in allowed range [0, 1, 2].")
		elif isinstance(n, (str, bytes)):
			if   n == 'x': return self.x
			elif n == 'y': return self.y
			elif n == 'z': return self.z
			else: raise IndexError("String index not in allowed range ['x', 'y', 'z'].")
		elif isinstance(n, slice):
			if n.start is None and n.stop is None and n.step is None:
				return self
			else:
				raise IndexError("Only the empty slice [:] is supported.")
		elif n is Ellipsis:
			return self
		else:
			raise TypeError("Unsupported index type.")
	
	def __setitem__(self, n, v):
		if isinstance(n, int):
			if   n == 0: self.x = v
			elif n == 1: self.y = v
			elif n == 2: self.z = v
			else: raise IndexError("Numeric index not in allowed range [0, 2].")
		elif isinstance(n, (str, bytes)):
			if   n == 'x': self.x = v
			elif n == 'y': self.y = v
			elif n == 'z': self.z = v
			else: raise IndexError("String index not in allowed range ['x', 'y', 'z'].")
		elif isinstance(n, slice):
			if n.start is None and n.stop is None and n.step is None:
				self.x = v[0]
				self.y = v[1]
				self.z = v[2]
			else:
				raise IndexError("Only the empty slice [:] is supported.")
		elif n is Ellipsis:
			self.x = v[0]
			self.y = v[1]
			self.z = v[2]
		else:
			raise TypeError("Unsupported index type.")
	
	def __iter__(self):
		"Iterate over the vector components."
		yield self.x
		yield self.y
		yield self.z
	
	def __eq__(self, other):
		"Vector euqality."
		return self.x == other.x and self.y == other.y and self.z == other.z
	
	def __neq__(self, other):
		"Vector ineuqality."
		return self.x != other.x or self.y != other.y or self.z != other.z
	
	def within_cube(self, halfedge):
		"Checks if the vector is contained within a cube of a given edge. The argument is half of the edge."
		return abs(self.x) <= halfedge and abs(self.y) <= halfedge and abs(self.z) <= halfedge
	
	def within_ball(self, radius):
		"Checks if the vector is contained within a ball of a given radius."
		return self.x ** 2 + self.y ** 2 + self.z ** 2 <= radius ** 2
	
	def __abs__(self):
		"Vector length."
		return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)
	
	def __pos__(self):
		"+ vector"
		return Vector(+self.x, +self.y, +self.z)
	
	def __neg__(self):
		"- vector"
		return Vector(-self.x, -self.y, -self.z)
	
	def vertor(self):
		"A vertor (vector with the same direction and length 1). For a zero vector returns a new zero vector."
		l = abs(self)
		if l > 0:
			return self / l
		else:
			return Vector.ZERO
	
	def power(self, e):
		return Vector(self.x ** e, self.y ** e, self.z ** e)
	
	def __add__(self, other):
		try:
			return Vector(*[getattr(self, _attr) + getattr(other, _attr) for _attr in "xyz"])
		except AttributeError:
			return NotImplemented
	
	def __sub__(self, other):
		try:
			return Vector(*[getattr(self, _attr) - getattr(other, _attr) for _attr in "xyz"])
		except AttributeError:
			return NotImplemented
	
	def __mul__(self, other):
		if isinstance(other, Vector):
			return sum([getattr(self, _attr) * getattr(other, _attr) for _attr in "xyz"])
		elif isinstance(other, Matrix):
			return other * self
		else:
			try:
				return Vector(*[getattr(self, _attr) * other for _attr in "xyz"])
			except:
				return NotImplemented
	
	def __truediv__(self, other):
		if isinstance(other, Matrix):
			return other.inverse() * self
		elif isinstance(other, Vector):
			raise TypeError("Invalid algebraic operation: division of a vector by a vector.")
		else:
			try:
				return Vector(*[getattr(self, _attr) / other for _attr in "xyz"])
			except ArithmeticError:
				raise
			except:
				return NotImplemented
	
	def __mod__(self, other):
		"Vector product"
		try:
			return Vector(self.y * other.z - self.z * other.y, self.z * other.x - self.x * other.z, self.x * other.y - self.y * other.x)
		except AttributeError:
			return NotImplemented
	
	def __rmul__(self, other):
		try:
			return Vector(*[other * getattr(self, _attr) for _attr in "xyz"])
		except:
			return NotImplemented
	
	def __and__(self, other):
		"Outer product"
		return Matrix(*[(getattr(self, i) * getattr(other, j)) for j in "xyz" for i in "xyz"])
	
	def __repr__(self):
		return "".join(["Vector(", ", ".join([repr(self.x), repr(self.y), repr(self.z)]), ")"])
	
	def __str__(self):
		return "".join(["〈", str(self.x), " ", str(self.y), " ", str(self.z), "〉"])
	
	def cross_product_rotation(self, m):
		r = m * Matrix.skew(self) * m.transpose()
		return Vector(r.yz, r.zx, r.xy)

Vector.ZERO = Vector(0.0, 0.0, 0.0)
Vector.X = Vector(1.0, 0.0, 0.0)
Vector.Y = Vector(0.0, 1.0, 0.0)
Vector.Z = Vector(0.0, 0.0, 1.0)


class Matrix:
	def __init__(self, xx, xy, xz, yx, yy, yz, zx, zy, zz):
		self.xx = xx
		self.xy = xy
		self.xz = xz
		self.yx = yx
		self.yy = yy
		self.yz = yz
		self.zx = zx
		self.zy = zy
		self.zz = zz
	
	#@staticmethod
	#def copy(other):
	#	return Matrix(other.xx, other.xy, other.xz, other.yx, other.yy, other.yz, other.zx, other.zy, other.zz)
	
	@staticmethod
	def skew(v):
		return Matrix(0.0, v.z, -v.y, -v.z, 0.0, v.x, v.y, -v.x, 0.0)
		#return Matrix.RX * v.x + Matrix.RY * v.y + Matrix.RZ * v.z
	
	@staticmethod
	def diag(v):
		return Matrix(v.x, 0.0, 0.0, 0.0, v.y, 0.0, 0.0, 0.0, v.z)
		#return Matrix.UX * v.x + Matrix.UY * v.y + Matrix.UZ * v.z
	
	def __hash__(self):
		return hash((0xa8521081, self.xx, self.xy, self.xz, self.yx, self.yy, self.yz, self.zx, self.zy, self.zz))
	
	def __len__(self):
		return 3
	
	def __getitem__(self, n):
		if isinstance(n, tuple) and len(n) == 2:
			x = n[0]
			y = n[1]
			
			if x is Ellipsis:
				x = None
			elif isinstance(x, int):
				if x == 0: x = 'x'
				elif x == 1: x = 'y'
				elif x == 2: x = 'z'
				else: raise IndexError("First numeric index not in allowed range [0, 1, 2].")
			elif isinstance(x, (str, bytes)):
				if x not in ['x', 'y', 'z']: raise IndexError("First string index not in allowed range ['x', 'y', 'z'].")
			elif isinstance(x, slice):
				if x.start is None and x.stop is None and x.step is None:
					x = None
				else:
					raise IndexError("Wrong slice value of the first index (use empty slice).")
			else:
				raise TypeError("Unsupported type of the first index (use int, string, empty slice or Ellipsis).")
			
			if y is Ellipsis:
				y = None
			elif isinstance(y, int):
				if y == 0: y = 'x'
				elif y == 1: y = 'y'
				elif y == 2: y = 'z'
				else: raise IndexError("Second numeric index not in allowed range [0, 1, 2].")
			elif isinstance(y, (str, bytes)):
				if y not in ['x', 'y', 'z']: raise IndexError("Second string index not in allowed range ['x', 'y', 'z'].")
			elif isinstance(y, slice):
				if y.start is None and y.stop is None and y.step is None:
					y = None
				else:
					raise IndexError("Wrong slice value of the second index (use empty slice).")
			else:
				raise TypeError("Unsupported type of the first index (use int, string, empty slice or Ellipsis).")
			
			if x is None and y is None:
				return self
			elif x is None:
				return Vector(getattr(self, 'x'+y), getattr(self, 'y'+y), getattr(self, 'z'+y))
			elif y is None:
				return Vector(getattr(self, x+'x'), getattr(self, x+'y'), getattr(self, x+'z'))
			else:
				return getattr(self, x+y)
			
		elif n is Ellipsis:
			return self
		else:
			raise TypeError("Index of unsupported type %s (use 2-tuple of ints, strings, empty slices or use the Ellipsis)." % str(type(n)))
	
	def __setitem__(self, n, v):
		if isinstance(n, tuple) and len(n) == 2:
			x = n[0]
			y = n[1]
			
			if x is Ellipsis:
				x = None
			elif isinstance(x, int):
				if x == 0: x = 'x'
				elif x == 1: x = 'y'
				elif x == 2: x = 'z'
				else: raise IndexError("First numeric index not in allowed range [0, 1, 2].")
			elif isinstance(x, (str, bytes)):
				if x not in ['x', 'y', 'z']: raise IndexError("First string index not in allowed range ['x', 'y', 'z'].")
			elif isinstance(x, slice):
				if x.start is None and x.stop is None and x.step is None:
					x = None
				else:
					raise IndexError("Wrong slice value of the first index (use empty slice).")
			else:
				raise TypeError("Unsupported type of the first index (use int, string, empty slice or Ellipsis).")
			
			if y is Ellipsis:
				y = None
			elif isinstance(y, int):
				if y == 0: y = 'x'
				elif y == 1: y = 'y'
				elif y == 2: y = 'z'
				else: raise IndexError("Second numeric index not in allowed range [0, 1, 2].")
			elif isinstance(y, (str, bytes)):
				if y not in ['x', 'y', 'z']: raise IndexError("Second string index not in allowed range ['x', 'y', 'z'].")
			elif isinstance(y, slice):
				if y.start is None and y.stop is None and y.step is None:
					y = None
				else:
					raise IndexError("Wrong slice value of the second index (use empty slice).")
			else:
				raise TypeError("Unsupported type of the first index (use int, string, empty slice or Ellipsis).")
			
			if x is None and y is None:
				for ni, i in enumerate('xyz'):
					for nj, j in enumerate('xyz'):
						setattr(self, i+j, v[ni][nj])
			elif x is None:
				for ni, i in enumerate('xyz'):
					setattr(self, i+y, v[ni])
			elif y is None:
				for nj, j in enumerate('xyz'):
					setattr(self, x+j, v[nj])
			else:
				for ni, i in enumerate('xyz'):
					for nj, j in enumerate('xyz'):
						setattr(self, i+j, v[ni][nj])
		elif n is Ellipsis:
			for ni, i in enumerate('xyz'):
				for nj, j in enumerate('xyz'):
					setattr(self, i+j, v[ni][nj])
		else:
			raise TypeError("Index of unsupported type %s (use 2-tuple of ints, strings, empty slices or use the Ellipsis)." % str(type(n)))
	
	def __iter__(self):
		yield Vector(self.xx, self.xy, self.xz)
		yield Vector(self.yx, self.yy, self.yz)
		yield Vector(self.zx, self.zy, self.zz)
	
	def __eq__(self, other):
		return all(getattr(self, _attr) == getattr(other, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz"))
	
	def __neq__(self, other):
		return any(getattr(self, _attr) != getattr(other, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz"))
	
	#def __abs__(self):
	#	"???"
	#	return (self.xx * self.yy * self.zz + self.xy * self.yz * self.zx + self.xz * self.yx * self.zy) \
	#	      - (self.xz * self.yy * self.zx + self.xx * self.yz * self.zy + self.xy * self.yx * self.zz)
	
	def __pos__(self):
		"copy of the matrix"
		return Matrix(*[+getattr(self, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
	
	def __neg__(self):
		"negative matrix"
		return Matrix(*[-getattr(self, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
	
	def __add__(self, other):
		"addition"
		try:
			return Matrix(*[getattr(self, _attr) + getattr(other, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
		except AttributeError:
			return NotImplemented
	
	def __sub__(self, other):
		"subtraction"
		try:
			return Matrix(*[getattr(self, _attr) - getattr(other, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
		except AttributeError:
			return NotImplemented
	
	def __mul__(self, other):
		if isinstance(other, Matrix):
			"multiplication by matrix"
			xx = self.xx * other.xx + self.xy * other.yx + self.xz * other.zx
			xy = self.xx * other.xy + self.xy * other.yy + self.xz * other.zy
			xz = self.xx * other.xz + self.xy * other.yz + self.xz * other.zz
			yx = self.yx * other.xx + self.yy * other.yx + self.yz * other.zx
			yy = self.yx * other.xy + self.yy * other.yy + self.yz * other.zy
			yz = self.yx * other.xz + self.yy * other.yz + self.yz * other.zz
			zx = self.zx * other.xx + self.zy * other.yx + self.zz * other.zx
			zy = self.zx * other.xy + self.zy * other.yy + self.zz * other.zy
			zz = self.zx * other.xz + self.zy * other.yz + self.zz * other.zz
			return Matrix(xx, xy, xz, yx, yy, yz, zx, zy, zz)
		elif isinstance(other, Vector):
			"multiplication by vector"
			x = self.xx * other.x + self.xy * other.y + self.xz * other.z
			y = self.yx * other.x + self.yy * other.y + self.yz * other.z
			z = self.zx * other.x + self.zy * other.y + self.zz * other.z
			return Vector(x, y, z)
		else:
			try:
				"multiplication by scalar"
				return Matrix(*[getattr(self, _attr) * other for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
			except ArithmeticError:
				raise
			except:
				return NotImplemented
	
	def __rmul__(self, other):
		"reversed multiplication"
		assert (not isinstance(other, Matrix)) and (not isinstance(other, Vector))
		try:
			"multiplication by scalar"
			return Matrix(*[other * getattr(self, _attr) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
		except ArithmeticError:
			raise
		except:
			return NotImplemented
	
	def __truediv__(self, other):
		try:
			"division by scalar"
			return Matrix(*[getattr(self, _attr) / other for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")])
		except ArithmeticError:
			raise
		except:
			return NotImplemented
	
	#def __rtruediv__(self, other):
	#	return other * self.inverse()
	
	#def __mod__(self, other):
	#	"???"
	#	if isinstance(other, Matrix):
	#		xx = self.xx * other.xx
	#		xy = self.xy * other.xy
	#		xz = self.xz * other.xz
	#		yx = self.yx * other.xx
	#		yy = self.yy * other.xy
	#		yz = self.yz * other.xz
	#		zx = self.zx * other.xx
	#		zy = self.zy * other.xy
	#		zz = self.zz * other.xz
	#		return Matrix(xx, xy, xz, yx, yy, yz, zx, zy, zz)
	#	else:
	#		return NotImplemented
	
	#def __abs__(self):
	#	"???"
	#	return math.sqrt(self.trace() ** 2)
	
	def __xor__(self, other):
		"Matrix commutator"
		return self * other - other * self
	
	def det(self):
		"Matrix determinant"
		return self.xx * self.yy * self.zz - self.xx * self.yz * self.zy \
		      + self.xy * self.yz * self.zx - self.xy * self.yx * self.zz \
		       + self.xz * self.yx * self.zy - self.xz * self.yy * self.zx
	
	def trace(self):
		"Trace of the matrix"
		return self.xx + self.yy + self.zz
	
	def inverse(self):
		"Inverse of the matrix"
		det = self.det()
		xx = (self.yy * self.zz - self.zy * self.yz) / det
		xy = (self.xz * self.zy - self.zz * self.xy) / det
		xz = (self.xy * self.yz - self.yy * self.xz) / det
		yx = (self.yz * self.zx - self.zz * self.yx) / det
		yy = (self.xx * self.zz - self.zx * self.xz) / det
		yz = (self.xz * self.yx - self.yz * self.xx) / det
		zx = (self.yx * self.zy - self.zx * self.yy) / det
		zy = (self.xy * self.zx - self.zy * self.xx) / det
		zz = (self.xx * self.yy - self.yx * self.xy) / det
		return Matrix(xx, xy, xz, yx, yy, yz, zx, zy, zz)
	
	def square(self):
		"Matrix square"
		return self * self
	
	def transpose(self):
		"Matrix transposition"
		return Matrix(self.xx, self.yx, self.zx, self.xy, self.yy, self.zy, self.xz, self.yz, self.zz)
	
	def symmetrize(self):
		"Symmetrized matrix"
		return self + self.transpose()
	
	def antisymmetrize(self):
		"Antisymmetrized matrix"
		return self - self.transpose()
	
	def small(self, precision=1.e-14):
		for x in 'xyz':
			for y in 'xyz':
				if abs(self[x, y]) >= precision:
					return False
		return True
	
	def singular_value_decomposition(self, precision=1.e-14):
		eps = 0.1 * precision
		tol = (0.1 * eps)**2 / eps
		
		u, q, v = svd([[self[_i, _j] for _j in "xyz"] for _i in "xyz"], eps=eps, tol=tol)
		
		u = Matrix(*sum(u, []))
		v = Matrix(*sum(v, []))
		q = Vector(*q)
		
		if u.det() < 0:
			u = -u
			v = -v
		
		return u, q, v
	
	def null_space(self, precision=1.e-14):
		u, q, v = self.singular_value_decomposition(precision=precision)
		c = []
		for n, s in enumerate(q):
			if s < precision:
				c.append(v[:, n])
		return c
	
	def eigenvalues(self, precision=1.e-14):
		if (self - Matrix.UNIT * self.xx).small(precision=precision**2): # special case: diagonal matrix with one triple eigenvalue
			return (self.xx, self.xx, self.xx)
		
		# Coefficients of the characteristic polynomial of this matrix: λ³ + bλ² + cλ + d = 0
		b = -self.trace()
		c = self.xx * self.zz + self.yy * self.zz + self.xx * self.yy - self.yz * self.zy - self.xy * self.yx - self.xz * self.zx
		d = -self.det()
		#print 1, b, c, d
		
		x = (3 * c - b**2) / 3
		y = (2 * b**3 - 9 * b * c + 27 * d) / 27
		z = y**2 / 4 + x**3 / 27
		
		if y**2 / 4 < z:
			raise ArithmeticError("This matrix does not have 3 eigenvalues: " + str(self))
		
		i = math.sqrt(y**2 / 4 - z)
		j = i**(1/3)
		
		if y == 0 and i == 0:
			k = 0
		elif -y > 2 * i:
			k = 0
		elif y > 2 * i:
			k = math.pi
		else:
			k = math.acos(-y / (2 * i))
		
		m = math.cos(k / 3)
		n = math.sqrt(3) * math.sin(k / 3)
		p = -b / 3
		
		return (2 * j * m + p), (-j * (m + n) + p), (-j * (m - n) + p)
	
	def eigenvectors(self, eigenvalues=None, precision=1.e-14):
		if eigenvalues is None:
			eigenvalues = self.eigenvalues(precision=precision)
		
		c = []
		for _e in eigenvalues:
			u, q, v = (self - _e * Matrix.UNIT).singular_value_decomposition(precision=precision)
			for n, s in enumerate(q):
				if s < precision:
					c.append(v[:, n])
		
		return c
	
	def eigendecomposition(self, precision=1.e-14):
		c = []
		d = []
		for e in set(self.eigenvalues(precision=precision)):
			u, q, v = (self - e * Matrix.UNIT).singular_value_decomposition(precision=precision)
			vb = [v[:, _n] for (_n, _s) in enumerate(q) if _s < precision]
			d.extend([_e] * len(vb))
			c.extend(vb)
		
		if len(d) != 3:
			raise ArithmeticError("This matrix does not have 3 eigenvectors: " + str(self))
		
		m = Matrix(c[0][0], c[1][0], c[2][0], c[0][1], c[1][1], c[2][1], c[0][2], c[1][2], c[2][2])
		return m, Vector(*d), m.inverse()
	
	def orthonormal_eigendecomposition(self, precision=1.e-14):
		es = self.eigenvalues(precision=precision) # get eigenvalues
		
		#print()
		c = []
		d = []
		for e in es:
			# find eigenvectors corresponding to the eigenvalue
			u, q, v = (self - e * Matrix.UNIT).singular_value_decomposition(precision=precision)
			# desired relative precision
			limit = 100 * min(q)
			vb = [v[:, _n] for (_n, _s) in enumerate(q) if _s <= limit]
			#print(limit, vb, q)
			
			if len(vb) == 2:
				vb[1] = vb[0] % vb[1] % vb[0]
			elif len(vb) == 3:
				vb = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
			
			while vb:
				vc = Vector(*vb.pop())
				for cc in c: # filter out redundant eigenvectors
					if (cc - vc).within_cube(math.sqrt(precision)):
						break
					if (cc + vc).within_cube(math.sqrt(precision)):
						break
				else:
					d.append(e)
					c.append(vc)
			
			#d.extend([e] * len(vb))
			#c.extend(vb)
		
		if len(d) != 3:
			raise ArithmeticError("This matrix can not be decomposed (try decreasing the precision): " + str(self) + ". Vectors: " + str(len(d)))
		
		cl = [c[0][0], c[1][0], c[2][0], c[0][1], c[1][1], c[2][1], c[0][2], c[1][2], c[2][2]]
		for n, v in enumerate(cl):
			if abs(v) < precision:
				cl[n] = 0
		m = Matrix(*cl)
		if m.det() < 0:
			m = -m
		return m, Vector(*d), m.transpose()
	
	def __repr__(self):
		return "".join(["Matrix(", ", ".join([repr(getattr(self, _attr)) for _attr in ("xx", "xy", "xz", "yx", "yy", "yz", "zx", "zy", "zz")]), ")"])
	
	def __str__(self):
		return "".join(["⟪", str(self.xx), " ", str(self.xy), " ", str(self.xz), " ⟊ ", str(self.yx), " ", str(self.yy), " ", str(self.yz), " ⟊ ", str(self.zx), " ", str(self.zy), " ", str(self.zz), "⟫"])
	
	def precession(self, m, n):
		r = (Matrix.skew(m) * self * Matrix.skew(n) * self.transpose()).antisymmetrize()
		return Vector(r.yz, r.zx, r.xy)

Matrix.ZERO = Matrix(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
Matrix.UNIT = Matrix(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0)
Matrix.RX = Matrix(0.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0)
Matrix.RY = Matrix(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0)
Matrix.RZ = Matrix(0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0)
Matrix.UX = Matrix(1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
Matrix.UY = Matrix(0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0)
Matrix.UZ = Matrix(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0)



